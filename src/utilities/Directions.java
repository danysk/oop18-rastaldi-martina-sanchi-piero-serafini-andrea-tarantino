package utilities;

/**
 * Enum representing the possible directions.
 * Andrea Serafini.
 *
 */
public enum Directions {
    /**
     * Direction selected.
     */
    UP, DOWN, LEFT, RIGHT;
}
