﻿##OOP18-minotaurus##
Lo scopo del gioco è quello di portare a destinazione presso un tempio segreto che si trova al centro del labirinto un numero di eroi (di default 2) dalla propria base. I giocatori potranno tentare di ostacolare gli avversari attraverso il minotauro il quale più "mangiare" le pedine degli altri giocatori riportandole alla base oppure spostando dei muri per bloccare il percorso.

features:

- modalità classica (due eroi da portare a destinazione, no hedgejumping)

- modalità hedgejumping (aggiunta caratteristica salta siepi)

- modalità personalizzata (possibilità di scelta di alcune regole di gioco)

- possibilità di salvare la partita in corso



Sviluppato da:
Piero Sanchi, Chiara Tarantino, Andrea Serafini, Martina Rastaldi

