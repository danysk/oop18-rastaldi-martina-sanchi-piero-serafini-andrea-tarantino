package controller.audio;

/**
 *
 * Piero Sanchi. An enum containing each audio.
 *
 */
public enum Audio {
    /**
     * Playable audios.
     */
    STEP, WIN, EVIL, TADA, WALL, DICE;
}
